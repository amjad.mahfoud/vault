package com.amjad.vault.app.api;

import com.amjad.vault.app.dto.TransactionRequestDTO;
import com.amjad.vault.app.dto.TransactionResponseDTO;
import com.amjad.vault.app.providers.TransactionProvider;
import com.amjad.vault.domain.models.TransactionModel;
import com.amjad.vault.infastructure.repositories.TransactionRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TransactionController {

    private final TransactionProvider transactionProvider;
    private final TransactionRepository transactionRepository;

    public TransactionController(TransactionProvider transactionProvider, TransactionRepository transactionRepository) {
        this.transactionProvider = transactionProvider;
        this.transactionRepository = transactionRepository;
    }

    @GetMapping("/transactions")
    public List<TransactionModel> transactions() {
        return this.transactionRepository.findAll();
    }

    @PostMapping("/transaction")
    public TransactionResponseDTO transaction(@RequestBody TransactionRequestDTO transactionRequestDTO) {
        return this.transactionProvider.recordTransaction(transactionRequestDTO);
    }
}
