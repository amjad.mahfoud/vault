package com.amjad.vault.app.api;

import com.amjad.vault.domain.models.LimitsModel;
import com.amjad.vault.infastructure.repositories.LimitsRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TransactionLimitController {
    final LimitsRepository depositLimitsRepository;

    public TransactionLimitController(LimitsRepository depositLimitsRepository) {
        this.depositLimitsRepository = depositLimitsRepository;
    }

    @GetMapping("/limits")
    public List<LimitsModel> transactionLimit() {
        return depositLimitsRepository.findAll();
    }
}
