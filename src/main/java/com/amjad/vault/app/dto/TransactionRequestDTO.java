package com.amjad.vault.app.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TransactionRequestDTO {
    private Long id;
    private Long customer_id;
    private String load_amount;
    private String time;

    public TransactionRequestDTO() {
    }

    public TransactionRequestDTO(Long id, Long customer_id, String load_amount, String time) {
        this.id = id;
        this.customer_id = customer_id;
        this.load_amount = load_amount;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public Double getLoad_amount() {
        return Double.parseDouble(load_amount.replace("$", ""));
    }

    public void setLoad_amount(String load_amount) {
        this.load_amount = load_amount;
    }

    public LocalDateTime getTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return LocalDateTime.parse(this.time, formatter);
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TransactionRequestDTO{" +
                "id=" + id +
                ", customer_id=" + customer_id +
                ", load_amount='" + load_amount + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
