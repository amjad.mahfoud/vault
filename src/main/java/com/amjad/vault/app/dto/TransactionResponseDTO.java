package com.amjad.vault.app.dto;

public class TransactionResponseDTO {
    private String id;
    private String customer_id;
    private Boolean accepted;

    public TransactionResponseDTO() {

    }

    public TransactionResponseDTO(String id, String customer_id, Boolean accepted) {
        this.id = id;
        this.customer_id = customer_id;
        this.accepted = accepted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }
}
