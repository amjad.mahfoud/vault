package com.amjad.vault.app.providers;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

import org.springframework.stereotype.Service;

import com.amjad.vault.app.dto.TransactionRequestDTO;
import com.amjad.vault.app.dto.TransactionResponseDTO;
import com.amjad.vault.domain.models.TransactionModel;
import com.amjad.vault.infastructure.services.LimitsValidationService;
import com.amjad.vault.infastructure.services.TransactionService;

import jakarta.transaction.Transactional;

@Service
public class TransactionProvider {
    private final TransactionService transactionService;
    private final LimitsValidationService limitsValidationService;

    public TransactionProvider(
            TransactionService transactionService,
            LimitsValidationService limitsValidationService) {
        this.transactionService = transactionService;
        this.limitsValidationService = limitsValidationService;
    }

    @Transactional
    public TransactionResponseDTO recordTransaction(TransactionRequestDTO transactionRequestDTO) {
        TransactionModel transaction = new TransactionModel(
                transactionRequestDTO.getId(),
                transactionRequestDTO.getLoad_amount(),
                transactionRequestDTO.getTime(),
                transactionRequestDTO.getCustomer_id());
        Double depositsThisDay = this.calculateDailyDeposits(transaction);
        Double depositsThisWeek = this.calculateWeeklyDeposits(transaction);


        boolean isDepositValid = this.limitsValidationService.isDepositValid(transaction, depositsThisDay,
                depositsThisWeek);


        if (isDepositValid) {
            this.transactionService.addTransaction(transaction);

            return new TransactionResponseDTO(transaction.getId().toString(), transaction.getUserId().toString(), true);
        }

        return new TransactionResponseDTO(transaction.getId().toString(), transaction.getUserId().toString(), false);

    }

    private Double calculateDailyDeposits(TransactionModel transaction) {
        LocalDateTime starDateTime = transaction.getTransactionDate().withHour(0).withMinute(0).withSecond(0)
                .withNano(0);
        LocalDateTime endDateTime = transaction.getTransactionDate().withHour(23).withMinute(59).withSecond(59)
                .withNano(999999999);

        return this.transactionService.calculateDeposits(transaction.getUserId(), starDateTime, endDateTime);
    }

    private Double calculateWeeklyDeposits(TransactionModel transaction) {
        LocalDateTime starDateTime = transaction.getTransactionDate()
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).withHour(0).withMinute(0)
                .withSecond(0)
                .withNano(0);

        LocalDateTime endDateTime = transaction.getTransactionDate()
                .with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)).withHour(23).withMinute(59)
                .withSecond(59)
                .withNano(999999999);

        return this.transactionService.calculateDeposits(transaction.getUserId(), starDateTime, endDateTime);
    }
}
