package com.amjad.vault.components;

import com.amjad.vault.domain.enums.LimitType;
import com.amjad.vault.domain.models.LimitsModel;
import com.amjad.vault.infastructure.repositories.LimitsRepository;

import jakarta.transaction.Transactional;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements CommandLineRunner {
    private final LimitsRepository repositLimitsRepository;

    public DataInitializer(LimitsRepository repositLimitsRepository) {
        this.repositLimitsRepository = repositLimitsRepository;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        LimitsModel entity1 = new LimitsModel(1L, 5000.0, LimitType.DAILY_MAX_AMOUNT);
        LimitsModel entity2 = new LimitsModel(2L, 20000.0, LimitType.WEEKLY_MAX_AMOUNT);
        LimitsModel entity3 = new LimitsModel(3L, 3.0, LimitType.DAILY_OPERATIONS);

       repositLimitsRepository.save(entity1);
       repositLimitsRepository.save(entity2);
       repositLimitsRepository.save(entity3);
    }
}