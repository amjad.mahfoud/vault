package com.amjad.vault.domain.enums;

public enum LimitType {
    DAILY_MAX_AMOUNT,
    WEEKLY_MAX_AMOUNT,
    MONTHLY_MAX_AMOUNT,
    DAILY_OPERATIONS
}
