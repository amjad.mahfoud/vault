package com.amjad.vault.domain.models;

import com.amjad.vault.domain.enums.LimitType;
import jakarta.persistence.*;

@Entity
@Table(name = "deposit_limits")
public class LimitsModel {
    @Id
    private Long id;

    @Column(name = "limit_Value")
    private Double limitValue;

    @Enumerated(EnumType.STRING)
    @Column(name = "limit_type")
    private LimitType limitType;

    public LimitsModel() {

    }

    public LimitsModel(Long id, Double limitValue, LimitType limitType) {
        this.id = id;
        this.limitValue = limitValue;
        this.limitType = limitType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Double getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(Double limitValue) {
        this.limitValue = limitValue;
    }

    public LimitType getLimitType() {
        return limitType;
    }

    public void setLimitType(LimitType limitType) {
        this.limitType = limitType;
    }
}
