package com.amjad.vault.infastructure.repositories;

import com.amjad.vault.domain.models.LimitsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LimitsRepository extends JpaRepository<LimitsModel, Long> {
}
