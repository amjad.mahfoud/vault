package com.amjad.vault.infastructure.repositories;

import com.amjad.vault.domain.models.TransactionModel;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<TransactionModel, Long> {
    List<TransactionModel> findByUserIdAndTransactionDateBetween(Long userId, LocalDateTime startDateTime, LocalDateTime endDateTime);
}