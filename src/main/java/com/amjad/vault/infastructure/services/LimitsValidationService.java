package com.amjad.vault.infastructure.services;

import java.util.List;

import com.amjad.vault.infastructure.repositories.LimitsRepository;
import org.springframework.stereotype.Service;

import com.amjad.vault.domain.enums.LimitType;
import com.amjad.vault.domain.models.LimitsModel;
import com.amjad.vault.domain.models.TransactionModel;

@Service
public class LimitsValidationService {

        private final LimitsRepository depositLimitsRepository;
        private final TransactionService transactionService;

        public LimitsValidationService(
                        TransactionService transactionService,
                        LimitsRepository depositLimitsRepository) {
                this.depositLimitsRepository = depositLimitsRepository;
                this.transactionService = transactionService;
        }

        public boolean isDepositValid(TransactionModel transaction, Double depositsThisDay, Double depositsThisWeek) {
                return this.isAmountBelowLimits(transaction, depositsThisDay, depositsThisWeek) && this.isDailyDepositsNumberValid(transaction);
        }

        public boolean isDailyDepositsNumberValid(TransactionModel transaction) {
                List<LimitsModel> limits = this.depositLimitsRepository.findAll();
                int maxDailyAllowedOperations = this.getDailyAllowedOperationsLimit(limits);
                int currentDayDepositOperationCount = this.transactionService.calculateNumberOfDeposits(transaction);

                return maxDailyAllowedOperations > currentDayDepositOperationCount;
        }

        private int getDailyAllowedOperationsLimit(List<LimitsModel> limits) {
                return limits.stream().filter(limit -> limit.getLimitType() == LimitType.DAILY_OPERATIONS)
                                .findFirst().orElseThrow().getLimitValue().intValue();
        }

        public boolean isAmountBelowLimits(TransactionModel transaction, Double depositsThisDay, Double depositsThisWeek) {
                Double amount = transaction.getAmount();
                
                List<LimitsModel> limits = this.depositLimitsRepository.findAll();
                Double dailyLimit = this.getDailyLimit(limits);
                Double weeklyLimit = this.getWeeklyLimit(limits);

                if (depositsThisDay + amount > dailyLimit) {
                        return false;
                }

                if (depositsThisWeek + amount > weeklyLimit) {
                        return false;
                }

                return true;
        }

        private Double getWeeklyLimit(List<LimitsModel> limits) {
                return limits.stream().filter(limit -> limit.getLimitType() == LimitType.WEEKLY_MAX_AMOUNT)
                                .findFirst().orElseThrow().getLimitValue();
        }

        private Double getDailyLimit(List<LimitsModel> limits) {
                return limits.stream().filter(limit -> limit.getLimitType() == LimitType.DAILY_MAX_AMOUNT)
                                .findFirst().orElseThrow().getLimitValue();
        }

}
