package com.amjad.vault.infastructure.services;

import com.amjad.vault.domain.models.TransactionModel;
import com.amjad.vault.infastructure.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<TransactionModel> transactions() {
        return this.transactionRepository.findAll();
    }

    public void addTransaction(TransactionModel transactionModel) {
        this.transactionRepository.save(transactionModel);
    }

    public Double calculateDeposits(Long userId, LocalDateTime from, LocalDateTime to) {
        return this.transactionRepository.findByUserIdAndTransactionDateBetween(userId,
                from, to).stream().mapToDouble(TransactionModel::getAmount).sum();
    }

    public int calculateNumberOfDeposits(TransactionModel transaction) {
        LocalDateTime starDateTime = transaction.getTransactionDate().withHour(0).withMinute(0).withSecond(0)
                .withNano(0);
        LocalDateTime endDateTime = transaction.getTransactionDate().withHour(23).withMinute(59).withSecond(59)
                .withNano(999999999);

        return this.transactionRepository.findByUserIdAndTransactionDateBetween(transaction.getUserId(),
                starDateTime, endDateTime).size();
    }
}
