package com.amjad.vault.infrastructure.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import com.amjad.vault.domain.enums.LimitType;
import com.amjad.vault.domain.models.LimitsModel;
import com.amjad.vault.domain.models.TransactionModel;
import com.amjad.vault.infastructure.repositories.LimitsRepository;
import com.amjad.vault.infastructure.services.LimitsValidationService;
import com.amjad.vault.infastructure.services.TransactionService;

@SpringBootTest
public class LimitsValidationServiceTest {
    @Mock
    private LimitsRepository limitsRepository;

    @Mock
    private TransactionService transactionService;

    private LimitsValidationService limitsValidationService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        limitsValidationService = new LimitsValidationService(transactionService, limitsRepository);

        // limits
        List<LimitsModel> limits = new ArrayList<>();
        limits.add(new LimitsModel(1l, 2.0, LimitType.DAILY_OPERATIONS));
        limits.add(new LimitsModel(1l, 1000.0, LimitType.DAILY_MAX_AMOUNT));
        limits.add(new LimitsModel(1l, 20000.0, LimitType.WEEKLY_MAX_AMOUNT));
        Mockito.when(limitsRepository.findAll()).thenReturn(limits);
    }

    @Test
    public void testIsDepositValidPasses() {
        TransactionModel transaction = new TransactionModel();
        transaction.setAmount(500.0);

        // Mock the transactionService to return a specific value
        Mockito.when(transactionService.calculateNumberOfDeposits(transaction)).thenReturn(1);

        boolean result = limitsValidationService.isDepositValid(transaction, 200.0, 4000.0);
        assertTrue(result);
    }

        @Test
    public void testIsDepositValidFails() {
        TransactionModel transaction = new TransactionModel();
        transaction.setAmount(500.0);

        // Mock the transactionService to return a specific value
        Mockito.when(transactionService.calculateNumberOfDeposits(transaction)).thenReturn(2);

        boolean result = limitsValidationService.isDepositValid(transaction, 200.0, 4000.0);
        assertFalse(result);
    }

    @Test
    public void testIsAmountBelowLimitsPasses() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startOfDay = now.withHour(0).withMinute(0).withSecond(0).withNano(0);
        LocalDateTime endOfDay = now.withHour(23).withMinute(59).withSecond(59).withNano(999999999);

        LocalDateTime startOfTheWeek = startOfDay.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDateTime endOfTheWeek = endOfDay.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));

        // Create a sample TransactionModel
        TransactionModel transaction = new TransactionModel(
                1L,
                100.0,
                LocalDateTime.now(),
                1L);

        Mockito.when(transactionService.calculateDeposits(transaction.getUserId(), startOfDay, endOfDay))
                .thenReturn(100.0);
        Mockito.when(transactionService.calculateDeposits(transaction.getUserId(), startOfTheWeek, endOfTheWeek))
                .thenReturn(500.0);

        boolean result = limitsValidationService.isAmountBelowLimits(transaction, 10.0, 50.0);

        assertEquals(true, result);
    }

    @Test
    public void testIsAmountBelowLimitsFails() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startOfDay = now.withHour(0).withMinute(0).withSecond(0).withNano(0);
        LocalDateTime endOfDay = now.withHour(23).withMinute(59).withSecond(59).withNano(999999999);

        LocalDateTime startOfTheWeek = startOfDay.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDateTime endOfTheWeek = endOfDay.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));

        // Create a sample TransactionModel
        TransactionModel transaction = new TransactionModel(
                1L,
                1000.0,
                LocalDateTime.now(),
                1L);

        Mockito.when(transactionService.calculateDeposits(transaction.getUserId(), startOfDay, endOfDay))
                .thenReturn(100.0);
        Mockito.when(transactionService.calculateDeposits(transaction.getUserId(), startOfTheWeek, endOfTheWeek))
                .thenReturn(500.0);

        boolean result = limitsValidationService.isAmountBelowLimits(transaction, 10.0, 50.0);

        assertEquals(false, result);
    }

    public void testIsDailyDepositsNumberValidPasses() {
                TransactionModel transaction = new TransactionModel(
                1L,
                1000.0,
                LocalDateTime.now(),
                1L);

        Mockito.when(transactionService.calculateNumberOfDeposits(transaction)).thenReturn(1);

        // Test when the daily deposits are within the limit
        boolean result = limitsValidationService.isDailyDepositsNumberValid(transaction);
        assertTrue(result);

        // Test when the daily deposits exceed the limit
        Mockito.when(transactionService.calculateNumberOfDeposits(transaction)).thenReturn(15);

        result = limitsValidationService.isDailyDepositsNumberValid(transaction);
        assertFalse(result);
    }


}
