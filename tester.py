import requests

url = 'http://localhost:8080/api/v1/transaction'
test_input_file = 'test_input.txt'
test_output_file = 'test_output.txt'
my_output_file = 'my_output.txt'

def send_request(url, data):
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.post(url, data=data, headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        return ''


def process_transaction_batch():
    with open(test_input_file, 'r') as file1:
        file1_lines = file1.readlines()

    with open(my_output_file, 'a') as file2:
        for line_num, (line1) in enumerate(file1_lines, start=1):
            res = send_request(url, line1.strip())
            if res != '':
                file2.write(f"{res}" + '\n')

    file2.close()
    print("Done!")
    
if __name__ == "__main__":
    process_transaction_batch()
